// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBOoZzi09nYSu7Lt2AMdUvQzfczpBNulLs",
    authDomain: "uas-ionic-9584f.firebaseapp.com",
    projectId: "uas-ionic-9584f",
    storageBucket: "uas-ionic-9584f.appspot.com",
    messagingSenderId: "1080624607175",
    appId: "1:1080624607175:web:7d938ff9f7f83d253573e2",
    measurementId: "G-BVNXKKL68R"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
