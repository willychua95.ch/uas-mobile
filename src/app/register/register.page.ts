import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertController, NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  validations_form: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';

  validation_messages = {
    'f_name': [
      {type: 'required', message: 'First Name is required'},
    ],
    'l_name': [
      {type: 'required', message: 'Last Name is required'},
    ],
    'email': [
      {type: 'required', message: 'Email is required'},
      {type: 'pattern', message: 'Enter a valid email'}
    ],
    'password': [
      {type: 'required', message: "Password is required"},
      {type: 'minLength', message: "Password must be at least 5 characters long"}
    ],
    'c_password': [
      {type: 'required', message: 'Confirm Password is required'},
      {type: 'minLength', message: "Confirm Password must be at least 5 characters long"}

    ],
    
  }

  constructor(
    private navCtrl: NavController, 
    private authSrv: AuthService, 
    private formBuilder: FormBuilder, 
    private alertController: AlertController, 
    private firestore: AngularFirestore,
  ) { 
    if(sessionStorage.getItem('uid')){
      navCtrl.navigateBack('/home/friendlist');
    }
  }

  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      f_name: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      l_name: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
      c_password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ]))
    });
  }


  tryRegister(value){
    if(value.c_password !== value.password){
      return this.presentAlert('Konfirmasi password salah');
    }
    
    this.authSrv.registerUser(value).then(res=>{
      console.log(res.user.uid);
      this.firestore.collection('users').doc(res.user.uid).set({
        f_name: value.f_name,
        l_name: value.l_name,
        image: null,
        friend: [],
        uid: res.user.uid,
      });
      console.log(res);
      this.goLoginPage();
    }, err=>{
      console.log(err);
      this.presentAlert(err);
      
    })
  }
  goLoginPage(){
    this.navCtrl.navigateBack('/login');
  }

  async presentAlert(message: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }


}
