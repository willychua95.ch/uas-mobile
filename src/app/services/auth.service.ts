import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  uid: string;

  constructor(private fireAuth: AngularFireAuth) { }

  registerUser(value){
    return new Promise<any>((resolve, reject)=>{
      this.fireAuth.createUserWithEmailAndPassword(value.email, value.password).then(res=>resolve(res), err=>reject(err));
    });
  }

  loginUser(value){
    return new Promise<any>((resolve, reject)=>{
      this.fireAuth.signInWithEmailAndPassword(value.email, value.password).then(res=>resolve(res), err => reject(err));
    })
  }

  logoutUser(){
    return new Promise((resolve, reject)=>{
        this.fireAuth.signOut().then(()=>{
          console.log('Log out');
          resolve;
        }).catch((error)=>{
          reject();
        })
      
    })
  }
  setUser(value){
    sessionStorage.setItem('uid', value);
  }
  getUser(){
    return sessionStorage.getItem('uid');
  }

  userDetails(){
    return this.fireAuth.user;
  }

}
