import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { SafeResourceUrl } from '@angular/platform-browser';
import { NavController, Platform } from '@ionic/angular';
import { Camera, CameraResultType, CameraSource, Capacitor } from '@capacitor/core';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  f_name: String;
  l_name: String;
  @ViewChild('filePicker', {static: false}) filePickerRef: ElementRef<HTMLInputElement>;
  photo: SafeResourceUrl;
  isDesktop: boolean;
  location: any;
  friend: any;

  constructor(
    private navCtrl: NavController,
    private firestore: AngularFirestore,
    private authSrv: AuthService,
    private platform: Platform,
    private storage: AngularFireStorage,
  ) { }

  ngOnInit() {
    const res = this.firestore.collection('users').doc(this.authSrv.getUser()).snapshotChanges().subscribe((res)=>{
      this.f_name = res.payload.data()['f_name'];
      this.l_name = res.payload.data()['l_name']
      this.photo = res.payload.data()['image'];
      this.friend = res.payload.data()['image'];
      
    })
    this.firestore.collection('location_user', (ref)=> ref.where('uid', '==', this.authSrv.getUser()).orderBy('time', 'desc')).snapshotChanges().subscribe((res)=>{
      this.location = res.map((doc)=>{
        return {
          id: doc.payload.doc.id,
          data: doc.payload.doc.data(),
        }
      });
      console.log(this.location);
    })
  }
  async getPicture(type: string){
    if(!Capacitor.isPluginAvailable('Camera') || (this.isDesktop && type == 'gallery')){
      this.filePickerRef.nativeElement.click();
      return;
    }

    const image = await Camera.getPhoto({
      quality: 100,
      width: 400,
      allowEditing: false,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Prompt
    })
    console.log(Image);
    this.photo = image.dataUrl;

    console.log('this.photo: ', this.photo);
    this.upload();
    //this.photo = this.sanitizer.bypassSecurityTrustUrl(image && (image.dataUrl));
  }

  onFileChoose(event: Event){
    const file = (event.target as HTMLInputElement).files[0];
    const pattern = /image-*/;
    const reader = new FileReader();

    if(!file.type.match(pattern)){
      console.log('File format not supported');
      return;
    }

    reader.onload = ()=>{
      this.photo = reader.result.toString();
    };
    reader.readAsDataURL(file);
  }

  dataURLtoFile(dataurl, filename){
    const arr = dataurl.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while(n--){
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, {type: mime});
  }
  upload(){
    const file = this.dataURLtoFile(this.photo, 'file');
    console.log('file:', file);
    const filePath = 'photos/'+ this.authSrv.getUser();
    const ref = this.storage.ref(filePath);
    const task = ref.put(file).then((res)=>{
      res.ref.getDownloadURL().then((response)=>{
        this.firestore.collection('users').doc(this.authSrv.getUser()).update({
          image: response,
        })
      })
    });
  }

  logout(){
    this.authSrv.logoutUser().then((res)=>{
      console.log('testing');
     
    })
    sessionStorage.removeItem('uid');

    this.navCtrl.navigateBack('/login');
  }
  del(id){
    console.log(id);
    this.firestore.collection('location_user').doc(id).delete();
  }
}
