import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  listFriend: Array<string>;
  dataFriend: any;
  constructor(
    private firestore: AngularFirestore,
    private authSrv: AuthService,
    private alertController: AlertController,
  ) { }

  ngOnInit() {
    this.firestore.collection('users').doc(this.authSrv.getUser()).snapshotChanges().subscribe((res)=>{
      this.listFriend = res.payload.data()['friend'];
      const notFriend = res.payload.data()['friend'];
      notFriend.push(this.authSrv.getUser());
      this.firestore.collection('users', ref=> ref.where('uid', "not-in", notFriend)).snapshotChanges().subscribe((test)=>{
        this.dataFriend = test.map((doc)=>{
          return doc.payload.doc.data();
        })
      });

    })
  }

  add(uid){
    console.log(this.listFriend);
    this.listFriend.push(uid);
    this.firestore.collection('users').doc(this.authSrv.getUser()).update({friend: this.listFriend}).then((res)=>{
      this.presentAlert('Teman berhasil ditambah');
    })
  }
  async presentAlert(message: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

}
