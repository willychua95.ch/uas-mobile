import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AlertController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-friendlist',
  templateUrl: './friendlist.page.html',
  styleUrls: ['./friendlist.page.scss'],
})
export class FriendlistPage implements OnInit {
  listFriend: Array<string>;
  dataFriend: any;

  constructor(
    private firestore: AngularFirestore,
    private authSrv: AuthService,
    private alertController: AlertController,
  ) { }

  ngOnInit() {
    this.firestore.collection('users').doc(this.authSrv.getUser()).snapshotChanges().subscribe((res)=>{
      this.listFriend = res.payload.data()['friend'];
      console.log(this.listFriend);
      if(this.listFriend != null && this.listFriend.length > 0){
        this.firestore.collection('users', ref=> ref.where('uid', "in", this.listFriend)).snapshotChanges().subscribe((test)=>{
          this.dataFriend = test.map((doc)=>{
            return doc.payload.doc.data();
          })
          console.log(this.dataFriend);
        });
      }else{
        this.dataFriend = [];
      }
    })
  }
  del(uid){
    const finalResult = this.listFriend.filter((ea)=>ea !== uid);
    this.firestore.collection('users').doc(this.authSrv.getUser()).update({friend: finalResult})
  }

}
