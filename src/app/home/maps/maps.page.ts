import { getInterpolationArgsLength } from '@angular/compiler/src/render3/view/util';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { NgForm } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';

declare var google: any;

@Component({
  selector: 'app-maps',
  templateUrl: './maps.page.html',
  styleUrls: ['./maps.page.scss'],
})
export class MapsPage implements OnInit {
  friends: any;
  map: any;
  location: any;
  infoWindow: any = new google.maps.InfoWindow();
  @ViewChild('map', {read: ElementRef, static:false}) mapRef: ElementRef;
  umnPos: any = {
    lat: -6.256081,
    lng: 106.618755
  };

  constructor(
    private firestore: AngularFirestore,
    private authSrv: AuthService,
    private alertController: AlertController,
  ) { }

  ngOnInit() {

    
  }
  getLocFriend(){
    this.firestore.collection('users').doc(this.authSrv.getUser()).snapshotChanges().subscribe((res)=>{
      this.friends =  res.payload.data()['friend'];
      console.log(this.friends);
      this.friends.map((isi)=>{
        
        this.firestore.collection('location_user', ref=>ref.where('uid', '==', isi).orderBy('time', 'desc').limit(1)).snapshotChanges().subscribe((response)=>{
            if(response.length > 0){
            const doc = response[0];
            console.log(doc.payload.doc.data());
            this.firestore.collection('users').doc(doc.payload.doc.data()['uid']).snapshotChanges().subscribe((test)=>{
              this.location = {
                user: test.payload.data(),
                lat: doc.payload.doc.data()['lat'],
                lng: doc.payload.doc.data()['lng'],
              }
              console.log(doc.payload.doc.data()['lat']);
              const markers = new google.maps.Marker({
                position: new google.maps.LatLng(doc.payload.doc.data()['lat'], doc.payload.doc.data()['lng']),
                title: 'test',
              })
              const pos = {
                lat: doc.payload.doc.data()['lat'],
                lng: doc.payload.doc.data()['lng']
              }
              console.log(markers)
          
              let newinfoWindow = new google.maps.InfoWindow();
              newinfoWindow.setPosition(pos);
              newinfoWindow.setContent(test.payload.data()['f_name'] + ' '+ test.payload.data()['l_name'] +'-'+ doc.payload.doc.data()['place']);
              newinfoWindow.open(this.map);
              
              
          });
        }
          
        });
      });
      
    });
  }

  ionViewDidEnter(){
    this.showMap(this.umnPos);
    this.getLocFriend();

    setInterval(()=>{
      this.updateLastLocation();
    },100000);
  }
  updateLastLocation(){
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition((position)=>{
        this.firestore.collection('location_user', (ref)=>ref.where('uid', '==', this.authSrv.getUser()).limit(1).orderBy('time', 'desc')).get().toPromise().then((res)=>{
          const data = res.docs[0].id;
          this.firestore.collection('location_user').doc(data).update({lat: position.coords.latitude, lng: position.coords.longitude, time: new Date()});
          console.log(data);
        })
      })
    }
  }

  showMap(pos: any){
    const location = new google.maps.LatLng(pos.lat, pos.lng);
    const options = {
      center: location,
      zoom: 13,
      disableDefaultUI: true,
    }
    this.map = new google.maps.Map(this.mapRef.nativeElement, options);
  }

  showCurrentLoc(form: NgForm){
    if(form.value.location == null || form.value.location === ""){
      return this.presentAlert("nama lokasi belum diinput");
    }
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition((position)=>{
        console.log(position);
        const pos={
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        console.log(this.authSrv.getUser());
        const now = new Date();
        console.log(now);
        this.firestore.collection('location_user').add({
          ...pos,
          place: form.value.location,
          uid: this.authSrv.getUser(),
          time: now,
        }).then((res)=>{
          this.presentAlert('Berhasil Check In');
        })
        this.infoWindow.setPosition(pos);
        this.infoWindow.setContent(form.value.location);
        this.infoWindow.open(this.map);
        this.map.setCenter(pos);
      })
    }
  }
  CurrentLoc(form: NgForm){
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition((position)=>{
        console.log(position);
        const pos={
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        this.infoWindow.setPosition(pos);
        this.infoWindow.setContent('Your Current Location');
        this.infoWindow.open(this.map);
        this.map.setCenter(pos);
      })
    }
  }
  async presentAlert(message: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }
}
